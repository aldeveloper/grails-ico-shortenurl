package shortenurl

class TinyUrlTests extends GroovyTestCase {

    def transactional = false

	public void test() {
      def shortUrl = TinyUrl.shorten("http://grails.org")
	  assertEquals "http://tinyurl.com/3xfpkv", shortUrl
	}

}
