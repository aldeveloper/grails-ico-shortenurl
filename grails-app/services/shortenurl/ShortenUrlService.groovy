package shortenurl

class ShortenUrlService {

    boolean transactional = false

    def tinyurl(String longUrl) {
      
	  def shortUrl = TinyUrl.shorten(longUrl)
	 
	  return shortUrl
    }

    def isgd(String longUrl) {

	  def shortUrl = IsGd.shorten(longUrl)
	  
	  if(shortUrl.contains("error")){
        log.error(shortUrl)
      }
      
	  return shortUrl
    }
}
